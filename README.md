# Personal Terminal
This is a simple script to initialize a brand new workstation with all the goodies 

## Prerequisite

* zsh
* oh-my-zsh

Optional: Set `zsh` as the default shell of the terminal

## Software that will be installed

* goenv
* jenv
* pyenv
* goenv
* powerlevel10k - ZSH theme

## Setup
1. Clone this repo
    
        git clone git@gitlab.com:nbugash/personal-toolchain.git ${HOME}/.personal-toolchain

2. Add the `initialization.sh` to the `.zshrc`

        echo '[[ -d ${HOME}/.personal-toolchain ]] && . ${HOME}/.personal-toolchain/initialization.sh' >> ${HOME}/.zshrc

3. (Optional) Re-source SHELL

        exec $SHELL && initAliases

## Example
Print all available commands 

    init:help

