#!/bin/sh

restartShell() {
    exec $SHELL
}

downloadRepo() {
    if [[ $# -lt 3 ]]; then
        echo "***************************************************"
        echo "USAGE: $0 [provider] [account] [repo] [download location]";
        echo "Example: $0 gitlab nbugash personal-powerlevel-10k ${HOME}/.powerlevel10k";
        echo "***************************************************"
        exit 1;
    fi 
    local provider=$1;
    local account=$2;
    local repo=$3;
    local download_loc=$4;
    [ -z "$download_loc" ] && download_loc=${HOME}/.${repo} || download_loc=$4
    echo "download location: ${download_loc}"
    case ${provider} in

        gitlab)
            git clone git@gitlab.com:${account}/${repo}.git ${download_loc}
            ;;
        github)
            git clone git@github.com:${account}/${repo}.git ${download_loc}
            ;;
        bitbucket)
            git clone git@bitbucket.com:${account}/${repo}.git ${download_loc}
            ;;
        *)
            echo "Unknown provider"
            ;;
    esac
}

installSdkman() {
    curl -s "https://get.sdkman.io" | zsh
    restartShell
}

installPowerlevel() {
    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git \ 
    ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
    echo "Don't forget to set the ZSH_THEME to 'powerlevel10k/powerlevel10k' in the .zshrc"
}

installJenv() {
    echo "installing jenv"
    local jenv_provider="github"
    local jenv_account="jenv"
    local jenv_repo="jenv"
    downloadRepo $jenv_provider $jenv_account $jenv_repo
    echo 'export JENV_ROOT="${HOME}/.jenv"' >> ~/.zshrc
    echo 'export PATH="${PATH}:${JENV_ROOT}/bin"' >> ~/.zshrc
    echo 'eval "$(jenv init -)"' >> ~/.zshrc
}

installRubyBuild() {
    local rb_provider="github";
    local rb_account="rbenv";
    local rb_repo="ruby-build";
    mkdir -p "$(rbenv root)"/plugins
    local rb_root="$(rbenv root)"/plugins/ruby-build
    downloadRepo $rb_provider $rb_account $rb_repo $rb_root
}

installRbenv() {
    echo "installing rbenv"
    local rbenv_provider="github"
    local rbenv_account="rbenv"
    local rbenv_repo="rbenv"
    local rbenv_root="${HOME}/.rbenv"
    downloadRepo $rbenv_provider $rbenv_account $rbenv_repo $rbenv_root
    echo 'To speed up rbenv run: cd ~/.rbenv && src/configure && make -C src'
    echo 'export RBENV_ROOT="${HOME}/.rbenv"' >> ~/.zshrc
    echo 'export PATH="${RBENV_ROOT}/bin:$PATH"' >> ~/.zshrc
    restartShell
    echo 'eval "$(rbenv init)"' >> ${HOME}/.zshrc
    installRubyBuild
    echo 'To verify rbenv run: "curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-doctor | bash"'
}

installPyenv() {
    echo "installing pyenv"
    local pyenv_provider="github"
    local pyenv_account="pyenv"
    local pyenv_repo="pyenv"
    downloadRepo $pyenv_provider $pyenv_account $pyenv_repo
    echo 'To speed up pyenv run: cd ~/.pyenv && src/configure && make -C src'
    echo 'export PYENV_ROOT="${HOME}/.pyenv"' >> ~/.zshrc
    echo 'export PATH="${PATH}:${PYENV_ROOT}/bin"' >> ~/.zshrc
    restartShell
    echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.zshrc
    restartShell
}

installGoenv() {
    echo "installing goenv"
    local goenv_provider="github"
    local goenv_account="syndbg"
    local goenv_repo="goenv"
    downloadRepo $goenv_provider $goenv_account $goenv_repo
    echo 'export GOENV_ROOT="$HOME/.goenv"' >> ${HOME}/.zshrc
    echo 'export PATH="$PATH:$GOENV_ROOT/bin"' >> ${HOME}/.zshrc
    echo 'eval "$(goenv init -)"' >> ${HOME}/.zshrc
    restartShell
    echo 'export PATH="$GOROOT/bin:$PATH"' >> ${HOME}/.zshrc
    echo 'export PATH="$PATH:$GOPATH/bin"' >> ${HOME}/.zshrc
    restartShell
}

printCommands() {
    echo "Here are the list of commands"
    echo "init:help => Print all available commands"
    echo "goenv:init => Install goenv"
    echo "goenv:install => Install the go compiler"
    echo "goenv:set:local => Set the go version locally"
    echo "goenv:set:global=> Set the go version globally"
    echo "goenv:versions => List all available versions"
    echo "rbenv:init => Install rbenv"
    echo "pyenv:init => Install pyenv"
    echo "jenv:init => Install jenv"
    echo "powerlevel:init => Install Powerlevel10K ZSH theme"
}

initAliases() { 
    echo '## Aliases' >> ${HOME}/.zshrc
    echo 'alias init:help="printCommands"' >> ${HOME}/.zshrc
    echo 'alias goenv:init="installGoenv"' >> ${HOME}/.zshrc
    echo 'alias goenv:install="goenv install"' >> ${HOME}/.zshrc
    echo 'alias goenv:set:local="goenv local"' >> ${HOME}/.zshrc
    echo 'alias goenv:set:global="goenv global"' >> ${HOME}/.zshrc
    echo 'alias goenv:versions="goenv install --list"' >> ${HOME}/.zshrc
    echo 'alias rbenv:init="installRbenv"' >> ${HOME}/.zshrc
    echo 'alias rbenv:install="rbenv install"' >> ${HOME}/.zshrc
    echo 'alias rbenv:versions="rbenv install --list"' >> ${HOME}/.zshrc
    echo 'alias rbenv:versions:all="rbenv install --list-all"' >> ${HOME}/.zshrc
    echo 'alias rbenv:set:local="rbenv local"' >> ${HOME}/.zshrc
    echo 'alias rbenv:set:global="rbenv global"' >> ${HOME}/.zshrc
    echo 'alias pyenv:init="installPyenv"' >> ${HOME}/.zshrc
    echo 'alias pyenv:install="pyenv install"' >> ${HOME}/.zshrc
    echo 'alias pyenv:versions="pyenv install --list"' >> ${HOME}/.zshrc
    echo 'alias pyenv:set:local="pyenv local"' >> ${HOME}/.zshrc
    echo 'alias pyenv:set:global="pyenv global"' >> ${HOME}/.zshrc
    echo 'alias jenv:init="installJenv"' >> ${HOME}/.zshrc
    echo 'alias jenv:add="jenv add"' >> ${HOME}/.zshrc
    echo 'alias jenv:versions="jenv versions"' >> ${HOME}/.zshrc
    echo 'alias jenv:set:local="jenv local"' >> ${HOME}/.zshrc
    echo 'alias jenv:set:global="jenv global"' >> ${HOME}/.zshrc
    echo 'alias powerlevel:init="installPowerlevel"' >> ${HOME}/.zshrc
    echo 'alias zshrc:source="source ${HOME}/.zshrc"' >> ${HOME}/.zshrc
    restartShell
}
